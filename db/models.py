from datetime import datetime
from typing import Any, List, Optional, Tuple

from sqlalchemy import Column, Date, Enum, ForeignKey, Integer, String, Table, delete, exists
from sqlalchemy.dialects.postgresql import TIMESTAMP
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload, relationship
from sqlalchemy.sql import ClauseElement
from sqlalchemy.sql.functions import current_timestamp

from api.schemas import Gender, Roles
from db.base import Base


class BaseModel(Base):
    __abstract__ = True
    __mapper_args__ = {"eager_defaults": True}

    id = Column(Integer, primary_key=True, autoincrement=True)
    created_at = Column(TIMESTAMP(timezone=False), server_default=current_timestamp())
    updated_at = Column(TIMESTAMP(timezone=False))

    def __str__(self):
        return f"<{type(self).__name__}({self.id=})>"

    async def save(
            self,
            session: AsyncSession,
    ):
        session.add(self)

    @classmethod
    def _get_query(
            cls,
            prefetch: Optional[Tuple[str, ...]] = None,
            options: Optional[List[Any]] = None
    ) -> Any:
        query = select(cls)
        if prefetch:
            if not options:
                options = []
            options.extend(joinedload(getattr(cls, x)) for x in prefetch)
            query = query.options(*options).execution_options(populate_existing=True)
        return query

    @classmethod
    async def get_or_create(
            cls,
            session: AsyncSession,
            **kwargs
    ):
        """Получение или создание объекта"""
        result = await session.execute(select(cls).filter_by(**kwargs))
        instance = result.unique().scalar_one_or_none()

        if instance:
            return instance, False

        else:
            params = {k: v for k, v in kwargs.items() if not isinstance(v, ClauseElement)}
            instance = cls(**params)

            try:
                session.add(instance)

            except:
                result = await session.execute(select(cls).filter_by(**kwargs))
                instance = result.unique().scalar_one()
                return instance, False

            else:
                return instance, True

    @classmethod
    async def get_or_none(
            cls,
            session: AsyncSession,
            **kwargs
    ):
        """Получение объекта или None"""
        result = await session.execute(select(cls).filter_by(**kwargs))
        return result.unique().scalar_one_or_none()

    @classmethod
    async def get_by_id(
            cls,
            session: AsyncSession,
            id: int,
    ):
        """Получение объекта по id"""
        return await session.scalar(select(cls).filter(cls.id == id))

    @classmethod
    async def get_objects(
            cls,
            session: AsyncSession,
            params: dict = None,
            sorts: list = None,
            prefetch: Optional[Tuple[str, ...]] = None,
            options: Optional[List[Any]] = None
    ):
        """Получение объектов"""
        if not params:
            params = {}
        if not sorts:
            sorts = [-cls.id]
        query = cls._get_query(prefetch, options).filter_by(**params).order_by(*sorts)
        result = await session.execute(query)
        return result.unique().scalars().all()

    @classmethod
    async def create_object(
            cls,
            session: AsyncSession,
            data: dict,
    ):
        """Создание объекта"""

        try:
            entity = cls(**data)
            session.add(entity)
            await session.flush()
            await session.refresh(entity)
            return entity, True
        except:
            return None, False

    async def update_object(
            self,
            data: dict,
    ):
        """Обновление объекта"""
        for k, v in data.items():
            setattr(self, k, v)
        self.updated_at = datetime.now()

    async def delete_object(
            self,
            session: AsyncSession,
    ):
        """Удаление объекта"""
        await session.delete(self)

    @classmethod
    async def delete_by_id(
            cls,
            session: AsyncSession,
            id: int
    ):
        """Удаление объекта по ID"""
        stmt = delete(cls).where(cls.id == id).returning(cls.id)
        result = await session.execute(stmt)
        result = result.scalar()
        if result:
            return True
        else:
            return False

    @classmethod
    async def exists(
            cls,
            session: AsyncSession,
            id: int,
    ):
        """Проверка на существование объекта по ID"""
        stmt = exists(cls).where(cls.id == id)
        return await session.scalar(select(stmt))


user_role = Table(
    'user_role',
    Base.metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('user_id', ForeignKey('user.id', ondelete='CASCADE'), nullable=False),
    Column('role_id', ForeignKey('role.id', ondelete='CASCADE'), nullable=False),
)


class Role(BaseModel):
    __tablename__ = 'role'

    name = Column(Enum(Roles), nullable=False, unique=True)
    users = relationship(
        'User',
        back_populates='roles',
        secondary=user_role,
        lazy='joined',
        uselist=True
    )

    async def to_dict(self, with_dates=False):
        data = {"id": self.id, "name": self.name}
        if with_dates:
            data["created_at"] = self.created_at.strftime("%Y-%m-%d %H:%M:%S")
            data["updated_at"] = self.updated_at.strftime("%Y-%m-%d %H:%M:%S") if self.updated_at else None
        return data


class User(BaseModel):
    __tablename__ = 'user'

    name = Column(String(length=50), nullable=False)
    surname = Column(String(length=100), nullable=False)
    login = Column(String(length=50), nullable=False, unique=True)
    password = Column(String(length=100), nullable=False)
    gender = Column(Enum(Gender), nullable=False)
    birthday = Column(Date, nullable=False)
    roles = relationship(
        'Role',
        back_populates='users',
        secondary=user_role,
        lazy='joined',
        uselist=True
    )

    async def to_dict(self):
        return {
            "id": self.id,
            "login": self.login,
            "name": self.name,
            "surname": self.surname,
            "gender": self.gender,
            "birthday": self.birthday.strftime("%d-%m-%Y"),
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
            "updated_at": self.updated_at.strftime("%Y-%m-%d %H:%M:%S") if self.updated_at else None,
            "roles": [await r.to_dict() for r in self.roles],
        }

    @classmethod
    async def get_users(
            cls,
            session: AsyncSession,
            prefetch: Optional[Tuple[str, ...]] = None,
            options: Optional[List[Any]] = None,
            filters: list = None,
            sorts: list = None,
            offset: Optional[int] = 0,
            limit: Optional[int] = 20,
    ):

        if not filters:
            filters = []

        if not sorts:
            sorts = [-cls.id]

        query = cls._get_query(prefetch, options).filter(*filters)
        query = query.order_by(*sorts).offset(offset).limit(limit)
        result = await session.execute(query)

        return result.unique().scalars().all()

    async def add_role(
            self,
            session: AsyncSession,
            role_name: str,
    ):
        """Добавление Роли Юзеру"""
        role, _ = await Role.get_or_create(session, **dict(name=role_name))
        if role not in self.roles:
            self.roles.append(role)

    @classmethod
    async def get_user_roles(
            cls,
            session: AsyncSession,
            user_id: int,
    ):
        """Получение Ролей Юзера"""
        user = await User.get_by_id(session, user_id)
        return [r.name for r in user.roles]

    @classmethod
    async def create_new_user(
            cls,
            session: AsyncSession,
            user_data: dict,
    ):
        """Создание нового юзера"""
        exist_stmt = exists(cls.login).where(cls.login == user_data.get("login"))
        result = await session.execute(select(exist_stmt))
        is_exists = result.scalar()
        if is_exists:
            return None, False
        user, created = await cls.create_object(session, user_data)
        return user, created
