from http import HTTPStatus

from aiohttp import web
from aiohttp.web_request import Request
from pydantic import ValidationError


def get_subclass_by_name(_class, name: str):
    if not name:
        return None
    return next((c for c in _class.__subclasses__() if c.__name__.lower() == name.lower()), None)


async def get_request_data(request: Request):
    if request.body_exists:
        try:
            body = await request.json()
        except:
            body = dict()
        return body
    else:
        return dict()


def validate_request_data(schema, request_data: dict):
    try:
        valid_data = schema(**request_data)
        return valid_data
    except ValidationError:
        raise web.HTTPBadRequest(text="Invalid request data")


def validate_filter_value(validator, value):
    try:
        valid_digit = validator(value=value)
        value = valid_digit.value
        return value
    except ValidationError:
        return


def get_id_from_req(request: Request):
    try:
        _id = request.match_info["id"]
        return int(_id)
    except:
        raise web.HTTPBadRequest(text=HTTPStatus.BAD_REQUEST.description)


def get_query_params(request: Request):
    return dict(request.query.items())
