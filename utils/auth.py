import enum
import hashlib
import json
import re
from typing import Any, Callable, Optional

from aiohttp import web
from aiohttp_security import AbstractAuthorizationPolicy
from aiohttp_security.api import AUTZ_KEY, IDENTITY_KEY
from aiohttp_session import AbstractStorage, Session, get_session
from sqlalchemy import exists, select

from api.schemas import Roles
from db import models
from db.models import Role, User, user_role
from logger import logger


class SimpleCookieStorage(AbstractStorage):
    """Simple JSON storage.

    Doesn't any encryption/validation, use it for tests only"""

    def __init__(
            self,
            *,
            cookie_name: str = "AIOHTTP_SESSION",
            domain: Optional[str] = None,
            max_age: Optional[int] = None,
            path: str = "/",
            secure: Optional[bool] = None,
            httponly: bool = True,
            samesite: Optional[str] = None,
            encoder: Callable[[object], str] = json.dumps,
            decoder: Callable[[str], Any] = json.loads,
    ) -> None:
        super().__init__(
            cookie_name=cookie_name,
            domain=domain,
            max_age=max_age,
            path=path,
            secure=secure,
            httponly=httponly,
            samesite=samesite,
            encoder=encoder,
            decoder=decoder,
        )

    @staticmethod
    def format_cookie(cookie) -> str:
        re.sub(r':\d\s?(?![{\[\s])([^,}]+)', r': "\1"', cookie)  # Add quotes to dict values
        cookie = re.sub(r'(\w+):', r'"\1":', cookie)  # Add quotes to dict keys
        cookie = re.sub(r'\[[^\]]+', lambda x: re.sub(r'(\d[\s\[])([^\],]+)', r'\1"\2"', x.group(0)), cookie)  # Add quotes to list items
        return cookie

    def load_cookie(self, request: web.Request) -> Optional[str]:
        cookie = request.cookies.get(self._cookie_name)
        return cookie

    async def load_session(self, request: web.Request) -> Session:
        cookie = self.load_cookie(request)
        if cookie is None:
            return Session(None, data=None, new=True, max_age=self.max_age)

        cookie = self.format_cookie(cookie)
        data = self._decoder(cookie)
        return Session(None, data=data, new=False, max_age=self.max_age)

    async def save_session(
            self, request: web.Request, response: web.StreamResponse, session: Session
    ) -> None:
        cookie_data = self._encoder(self._get_session_data(session))
        self.save_cookie(response, cookie_data, max_age=session.max_age)


class AuthorizationPolicy(AbstractAuthorizationPolicy):
    async def authorized_userid(self, request, identity):
        """
        Retrieve authorized user id.
        Return the user_id of the user identified by the identity
        or 'None' if no user exists related to the identity.
        """
        async with request.app['db']['session'] as session:
            stmt = exists(User.id).where(User.id == int(identity))
            identified = await session.scalar(select(stmt))

        logger.debug("User ID: %s, Identified: %s", identity, identified)
        if identified:
            return identity

    async def permits(self, request, identity, permission, context=None):
        """
        Check user permissions.
        Return True if the identity is allowed the permission
        in the current context, else return False.
        """
        if not identity:
            return False

        async with request.app['db']['session'] as session:
            stmt = select(Role.name).join(user_role, isouter=True).where(user_role.c.user_id == int(identity))
            result = await session.execute(stmt)
            user_roles = [r.name for r in result.scalars().all()]

        logger.debug("User ID: %s, Permission: %s, User Roles: %s", identity, permission, user_roles)
        if permission in user_roles:
            return True

        return False


async def authorized_userid(request):
    identity_policy = request.config_dict.get(IDENTITY_KEY)
    autz_policy = request.config_dict.get(AUTZ_KEY)
    if identity_policy is None or autz_policy is None:
        return None
    identity = await identity_policy.identify(request)
    if identity is None:
        return None  # non-registered user has None user_id
    user_id = await autz_policy.authorized_userid(request, identity)
    return user_id


async def permits(request, permission, context=None):
    assert isinstance(permission, (str, enum.Enum)), permission
    assert permission
    identity_policy = request.config_dict.get(IDENTITY_KEY)
    autz_policy = request.config_dict.get(AUTZ_KEY)
    if identity_policy is None or autz_policy is None:
        return True
    identity = await identity_policy.identify(request)
    # non-registered user still may has some permissions
    access = await autz_policy.permits(request, identity, permission, context)
    return access


async def check_permission(request, permission, context=None):
    """Checker that passes only to authoraised users with given permission.

    If user is not authorized - raises HTTPUnauthorized,
    if user is authorized and does not have permission -
    raises HTTPForbidden.
    """

    await check_authorized(request)
    allowed = await permits(request, permission, context)
    if not allowed:
        raise web.HTTPForbidden()


async def check_authorized(request):
    """Checker that raises HTTPUnauthorized for anonymous users.
    """
    userid = await authorized_userid(request)
    if userid is None:
        raise web.HTTPUnauthorized()
    return userid


def get_hashed_password(password: str) -> str:
    return hashlib.sha256(password.encode("utf-8")).hexdigest()


def has_access(password_out: str, password_in: str) -> bool:
    if password_out == password_in:
        return True
    else:
        return False


async def get_active_user_id(request):
    secure_session = await get_session(request)
    active_user = secure_session.get("AIOHTTP_SECURITY")
    logger.debug("Active User ID: %s", active_user)
    return int(active_user) if active_user else None


async def check_active_user(request, session, user_id):
    active_user_id = await get_active_user_id(request)
    user_exists = await models.User.exists(session, user_id)
    logger.debug("User Existence: %s", user_exists)
    if not user_exists:
        raise web.HTTPNotFound(text="User not found")

    active_user = await models.User.get_by_id(session, active_user_id)
    active_user_roles = [r.name for r in active_user.roles]
    logger.debug("Active User Roles: %s", active_user_roles)
    if user_id != active_user_id and Roles.admin not in active_user_roles:
        raise web.HTTPForbidden(text="Access denied. You do not have enough rights")
