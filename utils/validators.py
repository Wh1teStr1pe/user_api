from datetime import datetime, date

from pydantic import BaseModel, validator


class PaginationValidator(BaseModel):
    page: int
    quantity: int

    @validator('page')
    def page_must_be_positive(cls, v):
        if v <= 0:
            raise ValueError('Page must be greater than zero')
        return v

    @validator('quantity')
    def quantity_must_be_positive(cls, v):
        if v <= 0:
            raise ValueError('Quantity must be greater than zero')
        return v


class DigitFromStringValidator(BaseModel):
    value: str

    @validator('value')
    def value_must_be_an_integer(cls, v):
        if not v.isdigit():
            raise TypeError('Value must be an numeric')
        return int(v)


class DateFromStringValidator(BaseModel):
    value: str

    @validator('value')
    def must_be_valid_date_object(cls, v):
        try:
            v = datetime.strptime(v, "%d-%m-%Y")
            v = date(year=v.year, month=v.month, day=v.day)
            return v
        except:
            raise TypeError('Improper value for birthday argument')
