import json
from datetime import date
from json import JSONDecodeError
from typing import List

from sqlalchemy import Column
from sqlalchemy.orm import class_mapper

from db.models import BaseModel
from logger import logger
from utils import get_subclass_by_name, validate_filter_value
from .validators import DigitFromStringValidator, DateFromStringValidator, PaginationValidator


class BaseQueryParams:
    def __init__(
            self,
            page: int = 1,
            quantity: int = 10
    ):
        self.limit, self.offset = self.validate_pagination(page, quantity)

    @staticmethod
    def validate_pagination(page, quantity):
        pagination = PaginationValidator(page=page, quantity=quantity)
        offset = pagination.quantity * (pagination.page - 1)
        return pagination.quantity, offset

    @staticmethod
    def computed_operator(
            column: Column,
            operator: str,
            objects_type: str,
            value=None
    ):
        operators = {
            'gt': column.__gt__,
            'lt': column.__lt__,
            'ge': column.__ge__,
            'le': column.__le__,
            'eq': column.__eq__,
            'asc': column.asc,
            'desc': column.desc,
        }
        if objects_type == "filters":
            return operators.get(operator, column.__eq__)(value)

        elif objects_type == "sorts":
            return operators.get(operator, column.asc)()

    def create_binary_expressions(self, objects: List[dict], objects_type: str):
        expressions = list()
        for obj in objects:
            field, value = obj.get("field", ""), obj.get("value")
            model_name, operator = obj.get("model"), obj.get("operator")

            _class = get_subclass_by_name(BaseModel, model_name)
            if not _class:
                continue

            mapper = class_mapper(_class)
            if not hasattr(mapper.columns, field):
                continue

            column_type = mapper.columns[field].type.python_type
            if objects_type == "filters":

                if not value:
                    continue

                logger.debug("Column python type: %s", column_type)
                if column_type is int:
                    value = validate_filter_value(DigitFromStringValidator, value)
                    if not value:
                        continue

                elif column_type is date:
                    value = validate_filter_value(DateFromStringValidator, value)
                    if not value:
                        continue

            expression = self.computed_operator(
                column=mapper.columns[field],
                operator=operator,
                objects_type=objects_type,
                value=value
            )
            expressions.append(expression)

        return list(filter(lambda item: item is not None, expressions))

    def validate_query(self, query: str, objects_type: str):
        if not query:
            return None
        try:
            objects = json.loads(query).get(objects_type, list())
        except JSONDecodeError:
            return None

        expressions = self.create_binary_expressions(objects, objects_type)
        return expressions


class CommonQueryParams(BaseQueryParams):
    def __init__(
            self,
            page: int = 1,
            quantity: int = 10,
            filters: str = None,
            sorts: str = None,
    ):
        super().__init__(page, quantity)
        self.filters = self.validate_query(filters, "filters")
        self.sorts = self.validate_query(sorts, "sorts")
