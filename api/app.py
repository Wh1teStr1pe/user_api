from aiohttp import web
from aiohttp_security import SessionIdentityPolicy, setup
from aiohttp_session import session_middleware as sm

from db.base import async_session
from utils.auth import AuthorizationPolicy, SimpleCookieStorage, get_active_user_id
from .middleware import check_anonymity
from .routers.auth import auth_routes
from .routers.roles import roles_routes
from .routers.users import users_routes


async def make_app():
    # Applying Middlewares
    session_middleware = sm(SimpleCookieStorage())
    app = web.Application(
        middlewares=[
            session_middleware,
            check_anonymity,
        ]
    )

    # Adding routes
    async def root(request):
        greeting_phrase = "Hello There"
        greeting = dict(phrase=greeting_phrase)
        active_user = await get_active_user_id(request)
        if active_user:
            return web.json_response(greeting)
        else:
            greeting.update(phrase=f"{greeting_phrase}. Please login or register")
            return web.json_response(greeting)

    app.add_routes([web.get("/", root)])
    app.add_routes(auth_routes)
    app.add_routes(users_routes)
    app.add_routes(roles_routes)

    # Setting up Authorization Policy
    policy = SessionIdentityPolicy()
    setup(app, policy, AuthorizationPolicy())

    # CTX
    async def sa_context(app):
        async with async_session() as session:
            app['db'] = dict()
            app['db']['session'] = session
        yield

    app.cleanup_ctx.insert(0, sa_context)

    return app
