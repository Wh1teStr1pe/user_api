from datetime import date, datetime
from enum import Enum
from typing import Optional, Union

from pydantic import BaseModel, validator


class IDMixin(BaseModel):
    id: int


class Gender(str, Enum):
    female = 'female'
    male = 'male'


class Roles(str, Enum):
    admin = 'admin'
    basic = 'basic'


class Role(BaseModel):
    name: Roles


class UserRoleIn(Role):
    user_id: int


class UserBase(BaseModel):
    name: str
    surname: str
    gender: Gender
    birthday: Union[date, str]
    login: str

    class Config:
        orm_mode = True

    @validator('birthday', allow_reuse=True)
    def must_be_valid_date_object(cls, v):
        try:
            now = datetime.now()
            v = datetime.strptime(v, "%d-%m-%Y")
            if v > now:
                raise ValueError
            v = date(year=v.year, month=v.month, day=v.day)
            return v
        except:
            raise ValueError('Improper value for birthday argument')


class UserIn(UserBase):
    password: str

    @validator('password', allow_reuse=True)
    def password_length_must_be_more_than_eight_symbols(cls, v):
        if len(v) < 5:
            raise ValueError('Password is too short')
        return v


class UserLogIn(BaseModel):
    login: str
    password: str


class UserUpdateIn(UserBase):
    name: Optional[str]
    surname: Optional[str]
    birthday: Optional[str]
    gender: Optional[Gender]
    login: Optional[str]
    password: Optional[str]

    @validator('password', allow_reuse=True)
    def password_length_must_be_more_than_eight_symbols(cls, v):
        if len(v) < 5:
            raise ValueError('Password is too short')
        return v


class User(UserBase, IDMixin):
    password: str
    created_at: datetime
    updated_at: Optional[datetime]
