from aiohttp import web
from aiohttp.web_request import Request
from aiohttp_security import is_anonymous

from logger import logger


@web.middleware
async def check_anonymity(request: Request, handler):
    anonymous = await is_anonymous(request)
    logger.debug("Is anonymous: %s", anonymous)
    if anonymous is True and request.path == "/":
        return await handler(request)
    elif anonymous is True and request.path not in ["/login", "/register", "/logout"]:
        raise web.HTTPForbidden()
    else:
        return await handler(request)
