from aiohttp import web
from aiohttp.web_request import Request
from aiohttp_security import forget

from api import views

auth_routes = web.RouteTableDef()


@auth_routes.post("/login")
async def login(request: Request):
    response = await views.login(request)
    return response


@auth_routes.post("/register")
async def register(request: Request):
    response = await views.register(request)
    return response


@auth_routes.post("/logout")
async def logout(request: Request):
    response = web.HTTPFound("/")
    await forget(request, response)
    return response
