from aiohttp import web
from aiohttp.web_request import Request

from api import views

roles_routes = web.RouteTableDef()


@roles_routes.get("/roles")
async def get_roles(request: Request):
    roles = await views.get_roles(request)
    return web.json_response(roles)


@roles_routes.post("/roles")
async def create_role(request: Request):
    role = await views.create_role(request)
    return web.json_response(role)


@roles_routes.get("/roles/{id}")
async def get_role(request: Request):
    role = await views.get_role(request)
    return web.json_response(role)


@roles_routes.delete("/roles/{id}")
async def delete_role(request: Request):
    status = await views.delete_role(request)
    return web.json_response(status)
