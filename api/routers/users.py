from aiohttp import web
from aiohttp.web_request import Request

from api import views

users_routes = web.RouteTableDef()


@users_routes.get("/users")
async def get_users(request: Request):
    users = await views.get_users(request)
    return web.json_response(users)


@users_routes.post("/users")
async def create_user(request: Request):
    user = await views.create_user(request)
    return web.json_response(user)


@users_routes.get("/users/{id}")
async def get_user(request: Request):
    user = await views.get_user(request)
    return web.json_response(user)


@users_routes.put("/users/{id}")
async def update_user(request: Request):
    user = await views.update_user(request)
    return web.json_response(user)


@users_routes.delete("/users/{id}")
async def delete_user(request: Request):
    status = await views.delete_user(request)
    return web.json_response(status)


@users_routes.post("/users/add_role")
async def add_role(request: Request):
    user = await views.add_role(request)
    return web.json_response(user)
