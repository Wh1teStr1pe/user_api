from aiohttp import web
from aiohttp.web_request import Request
from aiohttp_security import remember

from api import schemas
from api.schemas import Roles
from db import models
from db.base import transaction
from logger import logger
from utils import get_id_from_req, get_query_params, get_request_data, validate_request_data
from utils.auth import check_active_user, check_permission, get_hashed_password, has_access
from utils.query import CommonQueryParams


async def get_users(request: Request) -> dict:
    await check_permission(request, Roles.basic)
    params = get_query_params(request)
    commons: CommonQueryParams = CommonQueryParams(
        page=params.get("page", 1),
        quantity=params.get("quantity", 10),
        filters=params.get("filters"),
        sorts=params.get("sorts"),
    )
    logger.debug("Query filters: %s", params.get("filters"))
    logger.debug("Query sorts: %s", params.get("sorts"))
    async with request.app['db']['session'] as session:
        async with transaction(session):
            users = await models.User.get_users(
                session=session,
                filters=commons.filters,
                sorts=commons.sorts,
                limit=commons.limit,
                offset=commons.offset,
                prefetch=("roles",)
            )
            if users:
                users = [await u.to_dict() for u in users]

    return {"users": users}


async def get_user(request: Request) -> dict:
    user_id = get_id_from_req(request)
    async with request.app['db']['session'] as session:
        await check_active_user(request, session, user_id)
        async with transaction(session):
            user = await models.User.get_by_id(session, user_id)
            user = await user.to_dict()
    return user


async def create_user(request: Request) -> dict:
    await check_permission(request, Roles.admin)
    request_data = await get_request_data(request)
    logger.debug("Request body: %s", {k: v for k, v in request_data.items() if k != "password"})

    user_data = validate_request_data(schemas.UserIn, request_data)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            user_data.password = get_hashed_password(user_data.password)
            user, created = await models.User.create_new_user(session, user_data.dict())
            logger.debug("User Created: %s", created)

            if not created:
                raise web.HTTPBadRequest(text="User with this username already exists")

            await user.add_role(session, Roles.basic)
            user = await user.to_dict()

    return user


async def update_user(request: Request) -> dict:
    user_id = get_id_from_req(request)
    request_data = await get_request_data(request)
    logger.debug("Request body: %s", {k: v for k, v in request_data.items() if k != "password"})
    user_data = validate_request_data(schemas.UserUpdateIn, request_data)
    async with request.app['db']['session'] as session:
        await check_active_user(request, session, user_id)
        async with transaction(session):
            user = await models.User.get_by_id(session, user_id)

            if user_data.password:
                user_data.password = get_hashed_password(user_data.password)

            await user.update_object(user_data.dict(exclude_none=True))
            user = await user.to_dict()
    return user


async def delete_user(request: Request) -> dict:
    user_id = get_id_from_req(request)
    async with request.app['db']['session'] as session:
        await check_active_user(request, session, user_id)
        async with transaction(session):
            status = await models.User.delete_by_id(session, user_id)
    return {"deleted": status}


async def add_role(request: Request) -> dict:
    await check_permission(request, Roles.admin)
    request_data = await get_request_data(request)
    logger.debug("Request body: %s", request_data)

    user_role_data = validate_request_data(schemas.UserRoleIn, request_data)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            user = await models.User.get_by_id(session, user_role_data.user_id)

            if not user:
                raise web.HTTPBadRequest(text="Invalid request data. User not found")

            await user.add_role(session, user_role_data.name)
            user = await user.to_dict()
    return user


async def get_roles(request: Request) -> dict:
    await check_permission(request, Roles.admin)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            roles = await models.Role.get_objects(session=session, prefetch=("users",))
            if roles:
                roles = [await r.to_dict(with_dates=True) for r in roles]
    return {"roles": roles}


async def create_role(request: Request) -> dict:
    await check_permission(request, Roles.admin)
    request_data = await get_request_data(request)
    logger.debug("Request body: %s", request_data)
    role_data = validate_request_data(schemas.Role, request_data)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            role, created = await models.Role.get_or_create(session, **role_data.dict())
            logger.debug("Role Created: %s", created)
            role = await role.to_dict(with_dates=True)
    return role


async def get_role(request: Request) -> dict:
    await check_permission(request, Roles.admin)
    role_id = get_id_from_req(request)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            role = await models.Role.get_by_id(session, role_id)
            if not role:
                raise web.HTTPNotFound(text="Role not found")

            role = await role.to_dict(with_dates=True)

    return role


async def delete_role(request: Request) -> dict:
    await check_permission(request, Roles.admin)
    role_id = get_id_from_req(request)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            status = await models.Role.delete_by_id(session, role_id)
    return {"deleted": status}


async def login(request: Request):
    request_data = await get_request_data(request)
    logger.debug("Request body: %s", {k: v for k, v in request_data.items() if k != "password"})

    user_data = validate_request_data(schemas.UserLogIn, request_data)

    async with request.app['db']['session'] as session:
        async with transaction(session):
            user = await models.User.get_or_none(session, **{"login": user_data.login})

        if not user:
            raise web.HTTPForbidden(text="User with provided login doesn't exists")

    password_out = get_hashed_password(user_data.password)
    valid = has_access(password_out, user.password)

    logger.debug("Login status: %s", valid)
    if not valid:
        raise web.HTTPForbidden(text="Wrong password")

    response = web.json_response(await user.to_dict())
    await remember(request, response, str(user.id))
    return response


async def register(request: Request):
    request_data = await get_request_data(request)
    logger.debug("Request body: %s", {k: v for k, v in request_data.items() if k != "password"})

    user_data = validate_request_data(schemas.UserIn, request_data)
    async with request.app['db']['session'] as session:
        async with transaction(session):
            user_data.password = get_hashed_password(user_data.password)
            user, created = await models.User.create_new_user(session, user_data.dict())

            logger.debug("User Created: %s", created)
            if not created:
                raise web.HTTPBadRequest(text="User with provided login already exists")

            await user.add_role(session, Roles.basic)

    response = web.json_response(await user.to_dict())
    await remember(request, response, str(user.id))
    return response
