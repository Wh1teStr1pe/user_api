#!/bin/bash

gunicorn api.app:make_app --bind 0.0.0.0:8000 --worker-class aiohttp.GunicornWebWorker --reload
