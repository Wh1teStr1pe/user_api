import os
from typing import Optional

from pydantic import BaseSettings, SecretStr


class BaseConfig(BaseSettings):
    """Base config."""

    DB_URL: SecretStr
    ECHO: bool = False

    class Config:
        """Loads the dotenv file"""
        env_file: str = ".env"


class DevConfig(BaseConfig):
    """Development config"""

    class Config:
        env_prefix: str = "DEV_"
        env_file: str = "local.env"


class ProdConfig(BaseConfig):
    """Production config"""

    class Config:
        env_prefix: str = "PROD_"


class StageConfig(BaseConfig):
    """Stage config"""

    class Config:
        env_prefix: str = "STAGE_"


class FactoryConfig:
    """Returns a config instance depending on the ENV_STATE variable"""

    def __init__(self, env_state: Optional[str]):
        self.env_state = env_state

    def __call__(self):
        if self.env_state == "dev":
            return DevConfig()

        elif self.env_state == "prod":
            return ProdConfig()

        elif self.env_state == "stage":
            return StageConfig()


config = FactoryConfig(env_state=os.getenv("ENV_STATE", "dev"))()
