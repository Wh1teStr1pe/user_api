import logging.config
from logger.config import config


logging.config.dictConfig(config=config)
logger = logging.getLogger('debug_logger')
