import logging


class FunctionFilter(logging.Filter):
    def filter(self, record):
        functions_names = [
            # '',
        ]

        if not functions_names:
            return True

        return record.funcName in functions_names


config = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'default': {
            'format': '%(asctime)s - %(levelname)s - %(name)s - %(module)s:%(funcName)s:%(lineno)s - %(message)s',
        }
    },


    'handlers': {
        'console_log': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'default',
            'filters': ['function_filter']
        }
    },

    'loggers': {
        'debug_logger': {
            'level': 'DEBUG',
            'handlers': ['console_log'],
            'propagate': False,
        }
    },

    'filters': {
        'function_filter': {
            '()': FunctionFilter,
        },
    },

}
