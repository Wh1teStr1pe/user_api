# Aiohttp Microservice for users

## users_api
Simple api with REST architecture and authentication

## Description

This is a fully async [Aiohttp](https://docs.aiohttp.org/en/stable/) project with async [SQLAlchemy ORM](https://docs.sqlalchemy.org/en/14/index.html).

The full stack of this project is composed by:

* [Aiohttp](https://fastapi.tiangolo.com/) - Asynchronous HTTP Client/Server for asyncio and Python.
* [SQLAlchemy ORM](https://docs.sqlalchemy.org/en/14/index.html) - SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.
* [PostgresSQL](https://www.postgresql.org) - PostgreSQL is a powerful, open source object-relational database system with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance.

## How to run project

### To run by using docker

 - Make sure you have installed `docker` and `docker-compose`
 - In order to run a project, initialize database and create superuser -  use `make all`
   
Superuser info:
- login: admin
- password: admin

## Settings
There are several types of config:
 - Dev
 - Stage
 - Prod

All variables store in `.env` file.
After running service, variables load to environment.
For each type of config there is a prefix for variables. After running service, variables parses by using BaseSettings class from pydantic.
