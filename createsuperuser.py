import asyncio
import sys

import click
from pydantic import ValidationError

from api.schemas import UserIn, Gender, Roles
from db.base import async_session
from db.models import User
from logger import logger
from utils.auth import get_hashed_password


async def create_super_user(superuser, password, name, surname):
    async with async_session() as session, session.begin():
        try:
            user_data = UserIn(
                login=superuser,
                password=password,
                name=name,
                surname=surname,
                gender=Gender.male,
                birthday="01-01-1970"
            )
            user_data.password = get_hashed_password(user_data.password)

        except ValidationError:
            sys.exit("User data is not valid")

        user, created = await User.create_new_user(session, user_data.dict())
        if created:
            await user.add_role(session, Roles.basic)
            await user.add_role(session, Roles.admin)
        else:
            logger.debug("User with provided login already exists")


@click.command(name="createsuperuser")
@click.argument('superuser', type=str)
@click.argument('password', type=str)
@click.option('--name', '-n', default='admin', help='First name of superuser', type=str)
@click.option('--surname', '-s', default='admin', help='Last name of superuser', type=str)
def main(superuser, password, name, surname):
    """
    A CLI that creates superuser.
    Here are an example:
    1. python3 createsuperuser.py admin admin
    """
    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_super_user(superuser, password, name, surname))


if __name__ == "__main__":
    main()
