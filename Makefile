all: build up stop migrate createsuperuser start

build:
	docker-compose -f docker-compose.dev.yml build
build-nocache:
	docker-compose -f docker-compose.dev.yml build --no-cache
up:
	docker-compose -f docker-compose.dev.yml up -d
start:
	docker-compose -f docker-compose.dev.yml start
restart:
	docker-compose -f docker-compose.dev.yml stop
	docker-compose -f docker-compose.dev.yml up
stop:
	docker-compose -f docker-compose.dev.yml stop
down:
	docker-compose -f docker-compose.dev.yml down
destroy:
	docker-compose -f docker-compose.dev.yml down -v
rm:
	docker-compose -f docker-compose.dev.yml rm -f
migrate:
	docker-compose -f docker-compose.dev.yml run app python -m alembic upgrade head
createsuperuser:
	docker-compose -f docker-compose.dev.yml run app python createsuperuser.py admin admin
dev:
	docker-compose -f docker-compose.dev.yml up --build
test:
	docker-compose -f docker-compose.dev.yml run app pytest $(if $m, -m $m)  $(if $k, -k $k) $o
devtest:
	docker-compose -f docker-compose.dev.yml run --volume=${PWD}/.:/app/ app pytest -s $(if $m, -m $m)  $(if $k, -k $k) $o

.PHONY: all build build-nocache up start down destroy migrate createsuperuser stop dev devtest