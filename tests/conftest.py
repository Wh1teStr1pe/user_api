import asyncio
from typing import Any, Generator

import pytest
from aiohttp.test_utils import TestClient
from faker import Faker
from faker.providers import DynamicProvider
from sqlalchemy.ext.asyncio import AsyncConnection, AsyncSession

from api.schemas import Gender
from db.base import Base, async_session, engine
from tests.utils import create_user, get_user_data


@pytest.fixture(scope="session")
def event_loop(request) -> Generator:
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="function")
def fake():
    gender_provider = DynamicProvider(
        provider_name="gender",
        elements=[Gender.male.value, Gender.female.value],
    )
    fake: Faker = Faker('en_US')
    fake.add_provider(gender_provider)
    yield fake


@pytest.fixture(scope="function")
async def db_session() -> AsyncSession:
    async with engine.begin() as connection:
        connection: AsyncConnection
        await connection.run_sync(Base.metadata.drop_all)
        await connection.run_sync(Base.metadata.create_all)
        async with async_session(bind=connection) as session:
            yield session
            await session.flush()
            await session.rollback()
        await connection.rollback()


@pytest.fixture()
def _sa_context(db_session: AsyncSession):
    async def _override_get_session(app):
        app['db'] = dict()
        app['db']['session'] = db_session
        yield

    return _override_get_session


@pytest.fixture()
async def _app(_sa_context):
    from api.app import make_app
    app = await make_app()
    app.cleanup_ctx.pop(0)
    app.cleanup_ctx.insert(0, _sa_context)
    return app


@pytest.fixture()
async def client(aiohttp_client: Any, _app) -> TestClient:
    yield await aiohttp_client(_app)


@pytest.fixture()
async def admin(db_session, fake):
    admin_data = get_user_data(
        fake=fake,
        birthday="01-01-1970",
        login="admin",
        password="admin"
    )
    admin = await create_user(db_session, admin_data, admin=True)
    yield admin


@pytest.fixture()
async def main_user(db_session, fake):
    user_data = get_user_data(fake=fake, password="admin")
    yield await create_user(db_session, user_data)
