from faker import Faker

from api import schemas
from db.models import User
from utils.auth import get_hashed_password


def get_user_data(
        fake: Faker,
        name: str = None,
        surname: str = None,
        gender: str = None,
        birthday: str = None,
        login: str = None,
        password: str = None
):
    return dict(
        name=fake.first_name() if not name else name,
        surname=fake.last_name() if not surname else surname,
        gender=fake.gender() if not gender else gender,
        birthday=fake.date_of_birth(minimum_age=20, maximum_age=80).strftime("%d-%m-%Y") if not birthday else birthday,
        login=fake.user_name() if not login else login,
        password=fake.password() if not password else password,
    )


async def create_user(db_session, data, admin=False):
    valid_data = schemas.UserIn(**data)
    valid_data.password = get_hashed_password(valid_data.password)
    user, created = await User.create_object(db_session, valid_data.dict())
    if created:
        if admin is True:
            await user.add_role(db_session, schemas.Roles.admin)

        await user.add_role(db_session, schemas.Roles.basic)
        await db_session.commit()
    return user
