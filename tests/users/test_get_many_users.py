import pytest
from aiohttp.test_utils import TestClient
from sqlalchemy.future import select

from db.models import User

pytestmark = [pytest.mark.asyncio, pytest.mark.users]


async def test_get_users_by_authorized_admin(client: TestClient, db_session, authorized_admin, main_user):
    response = await client.get("users")
    assert response.status == 200
    data = await response.json()
    users = data.get("users")
    users_ids = [u.get("id") for u in users]
    result = await db_session.execute(select(User.id).filter(User.id.in_(users_ids)).order_by(-User.id))
    users_in_db = result.scalars().all()
    assert users is not None
    assert users_ids == users_in_db


async def test_get_users_by_authorized_user(client: TestClient, db_session, authorized_user):
    response = await client.get("users")
    assert response.status == 200
    data = await response.json()
    users = data.get("users")
    users_ids = [u.get("id") for u in users]
    result = await db_session.execute(select(User.id).filter(User.id.in_(users_ids)).order_by(-User.id))
    users_in_db = result.scalars().all()
    assert users is not None
    assert users_ids == users_in_db


async def test_get_users_by_unauthorized_user(client: TestClient, db_session, main_user):
    response = await client.get("users")
    assert response.status == 403
