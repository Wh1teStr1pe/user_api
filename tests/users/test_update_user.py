import random
from datetime import date

import pytest
from aiohttp.test_utils import TestClient

from db.models import User
from tests.utils import get_user_data
from utils.auth import get_hashed_password

pytestmark = [pytest.mark.asyncio, pytest.mark.users]


async def test_update_basic_user_by_authorized_admin(client: TestClient, db_session, fake, authorized_admin, main_user):
    data = get_user_data(fake)
    response = await client.put(f"users/{main_user.id}", json=data)
    assert response.status == 200
    response_data = await response.json()
    for k in list(data.keys()):
        updated_user = await User.get_by_id(db_session, main_user.id)
        user_attr = getattr(updated_user, k)
        if isinstance(user_attr, date):
            user_attr = user_attr.strftime("%d-%m-%Y")
        if k == "password":
            data[k] = get_hashed_password(data[k])
        assert user_attr == data[k]

    assert response_data.get("updated_at") is not None


async def test_update_user_if_not_exists_by_authorized_admin(client: TestClient, db_session, authorized_admin):
    user_id = random.randint(2, 1000)
    response = await client.put(f"users/{user_id}")
    assert response.status == 404
    assert await User.exists(db_session, user_id) is False
    assert await response.text() == "User not found"


async def test_update_self_profile_when_authorized(client: TestClient, db_session, fake, authorized_user):
    data = get_user_data(fake, birthday=authorized_user.birthday.strftime("%d-%m-%Y"))
    response = await client.put(f"users/{authorized_user.id}", json=data)
    assert response.status == 200
    response_data = await response.json()
    for k in list(data.keys()):
        updated_user = await User.get_by_id(db_session, authorized_user.id)
        user_attr = getattr(updated_user, k)
        if isinstance(user_attr, date):
            user_attr = user_attr.strftime("%d-%m-%Y")
        if k == "password":
            data[k] = get_hashed_password(data[k])
        assert user_attr == data[k]

    assert response_data.get("updated_at") is not None


async def test_update_basic_user_by_unauthorized_user(client: TestClient, db_session, fake, main_user):
    data = dict(login=fake.user_name())
    response = await client.put(f"users/{main_user.id}", json=data)
    assert response.status == 403
    for k in list(data.keys()):
        updated_user = await User.get_by_id(db_session, main_user.id)
        user_attr = getattr(updated_user, k)
        if isinstance(user_attr, date):
            user_attr = user_attr.strftime("%d-%m-%Y")
        if k == "password":
            data[k] = get_hashed_password(data[k])
        assert user_attr != data[k]


async def test_update_user_if_wrong_birthday_by_authorized_admin(client: TestClient, db_session, fake, authorized_user, main_user):
    data = dict(password=fake.password(length=4))
    response = await client.put(f"users/{main_user.id}", json=data)
    assert response.status == 400
    assert await response.text() == "Invalid request data"
    for k in list(data.keys()):
        updated_user = await User.get_by_id(db_session, main_user.id)
        user_attr = getattr(updated_user, k)
        if isinstance(user_attr, date):
            user_attr = user_attr.strftime("%d-%m-%Y")
        if k == "password":
            data[k] = get_hashed_password(data[k])
        assert user_attr != data[k]


async def test_update_user_if_wrong_password_by_authorized_admin(client: TestClient, db_session, fake, authorized_user, main_user):
    data = dict(birthday=str(fake.date_of_birth(minimum_age=20, maximum_age=80).year))
    response = await client.put(f"users/{main_user.id}", json=data)
    assert response.status == 400
    assert await response.text() == "Invalid request data"
    for k in list(data.keys()):
        updated_user = await User.get_by_id(db_session, authorized_user.id)
        user_attr = getattr(updated_user, k)
        if isinstance(user_attr, date):
            user_attr = user_attr.strftime("%d-%m-%Y")
        if k == "password":
            data[k] = get_hashed_password(data[k])
        assert user_attr != data[k]
