import random

import pytest
from aiohttp.test_utils import TestClient
from sqlalchemy.future import select
from sqlalchemy.sql.functions import count

from db.models import User, user_role

pytestmark = [pytest.mark.asyncio, pytest.mark.users]


async def test_delete_basic_user_by_authorized_admin(client: TestClient, db_session, authorized_admin, main_user):
    users = await User.get_users(db_session)
    expected_user_count = len(users) - 1
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt) - 1
    response = await client.delete(f"users/{main_user.id}")
    assert response.status == 200
    data = await response.json()
    assert data.get("deleted") is True
    assert await User.exists(db_session, main_user.id) is False
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count


async def test_delete_user_if_not_exists_by_authorized_admin(client: TestClient, db_session, authorized_admin):
    user_id = random.randint(2, 1000)
    response = await client.delete(f"users/{user_id}")
    assert response.status == 404
    assert await User.exists(db_session, user_id) is False
    assert await response.text() == "User not found"


async def test_delete_self_profile_when_authorized(client: TestClient, db_session, authorized_user):
    users = await User.get_users(db_session)
    expected_user_count = len(users) - 1
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt) - 1
    response = await client.delete(f"users/{authorized_user.id}")
    assert response.status == 200
    data = await response.json()
    assert data.get("deleted") is True
    assert await User.exists(db_session, authorized_user.id) is False
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count


async def test_delete_basic_user_by_unauthorized_user(client: TestClient, db_session, main_user):
    users = await User.get_users(db_session)
    expected_user_count = len(users)
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt)
    response = await client.delete(f"users/{main_user.id}")
    assert response.status == 403
    assert await User.exists(db_session, main_user.id) is True
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count
