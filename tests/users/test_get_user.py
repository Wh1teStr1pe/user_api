import random

import pytest
from aiohttp.test_utils import TestClient

from api.schemas import Roles
from db.models import User

pytestmark = [pytest.mark.asyncio, pytest.mark.users]


async def test_get_admin_profile_by_authorized_admin(client: TestClient, db_session, authorized_admin):
    """
    Successful Response Example:
    {
        "id": 1,
        "login": "admin",
        "name": "admin",
        "surname": "admin",
        "gender": "male",
        "birthday": "01-01-1970",
        "created_at": "2022-06-05 10:05:27",
        "updated_at": null,
        "roles": [
            {
                "id": 1,
                "name": "basic"
            },
            {
                "id": 2,
                "name": "admin"
            }
        ]
    }
    """
    response = await client.get(f"users/{authorized_admin.id}")
    assert response.status == 200
    data = await response.json()
    user_roles = await User.get_user_roles(db_session, authorized_admin.id)
    for k in list(data.keys()):
        user_attr = getattr(authorized_admin, k)
        if k == "birthday":
            user_attr = user_attr.strftime("%d-%m-%Y")
        elif k in ["created_at", "updated_at"]:
            if data[k]:
                user_attr = user_attr.strftime("%Y-%m-%d %H:%M:%S")
        elif k == "roles":
            assert len(data[k]) == 2
            for role in data[k]:
                assert role["name"] in user_roles
            continue
        assert user_attr == data[k]


async def test_get_other_user_profile_by_authorized_admin(client: TestClient, db_session, authorized_admin, main_user):
    """
    Successful Response Example:
    {
        "id": 2,
        "login": "random",
        "name": "random",
        "surname": "random",
        "gender": "male",
        "birthday": "01-01-1970",
        "created_at": "2022-06-05 10:05:27",
        "updated_at": null,
        "roles": [
            {
                "id": 1,
                "name": "basic"
            }
        ]
    }
    """
    response = await client.get(f"users/{main_user.id}")
    assert response.status == 200
    data = await response.json()
    user_roles = await User.get_user_roles(db_session, main_user.id)
    for k in list(data.keys()):
        user_attr = getattr(main_user, k)
        if k == "birthday":
            user_attr = user_attr.strftime("%d-%m-%Y")
        elif k in ["created_at", "updated_at"]:
            if data[k]:
                user_attr = user_attr.strftime("%Y-%m-%d %H:%M:%S")
        elif k == "roles":
            assert len(data[k]) == 1
            for role in data[k]:
                assert role["name"] in user_roles
            continue
        assert user_attr == data[k]


async def test_get_self_profile_by_authorized_user(client: TestClient, db_session, authorized_user):
    """
    Successful Response Example:
    {
        "id": 2,
        "login": "random",
        "name": "random",
        "surname": "random",
        "gender": "male",
        "birthday": "01-01-1970",
        "created_at": "2022-06-05 10:05:27",
        "updated_at": null,
        "roles": [
            {
                "id": 1,
                "name": "basic"
            }
        ]
    }
    """
    response = await client.get(f"users/{authorized_user.id}")
    assert response.status == 200
    data = await response.json()
    user_roles = await User.get_user_roles(db_session, authorized_user.id)
    for k in list(data.keys()):
        user_attr = getattr(authorized_user, k)
        if k == "birthday":
            user_attr = user_attr.strftime("%d-%m-%Y")
        elif k in ["created_at", "updated_at"]:
            if data[k]:
                user_attr = user_attr.strftime("%Y-%m-%d %H:%M:%S")
        elif k == "roles":
            assert len(data[k]) == 1
            for role in data[k]:
                assert role["name"] in user_roles
            continue
        assert user_attr == data[k]


async def test_get_user_if_not_exists_by_authorized_user(client: TestClient, db_session, authorized_admin):
    user_id = random.randint(2, 1000)
    response = await client.get(f"users/{user_id}")
    assert response.status == 404
    assert await User.exists(db_session, user_id) is False
    assert await response.text() == "User not found"


async def test_get_user_by_unauthorized_user(client: TestClient, db_session, main_user):
    response = await client.get(f"users/{main_user.id}")
    assert response.status == 403
