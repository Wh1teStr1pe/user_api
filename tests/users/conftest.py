import pytest

from api import schemas


@pytest.fixture()
async def authorized_admin(admin, client):
    user_data = schemas.UserLogIn(login=admin.login, password="admin")
    response = await client.post("login", json=user_data.dict())
    data = await response.json()
    assert response.status == 200
    assert data.get("login") == admin.login
    yield admin


@pytest.fixture()
async def authorized_user(main_user, client):
    user_data = schemas.UserLogIn(login=main_user.login, password="admin")
    response = await client.post("login", json=user_data.dict())
    data = await response.json()
    assert response.status == 200
    assert data.get("login") == main_user.login
    yield main_user
