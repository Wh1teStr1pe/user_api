import pytest
from aiohttp.test_utils import TestClient
from sqlalchemy.future import select
from sqlalchemy.sql.functions import count

from api.schemas import Roles
from db.models import User, user_role
from tests.utils import get_user_data

pytestmark = [pytest.mark.asyncio, pytest.mark.users]


async def test_create_user_by_authorized_admin(client: TestClient, db_session, authorized_admin, fake):
    user_data = get_user_data(fake)
    expected_user_count = len(await User.get_users(db_session)) + 1
    response = await client.post("users", json=user_data)
    assert response.status == 200
    data = await response.json()
    assert data.get("login") == user_data.get("login")
    assert await User.exists(db_session, data.get("id")) is True
    assert len(await User.get_users(db_session)) == expected_user_count
    user_roles = await User.get_user_roles(db_session, data.get("id"))
    assert Roles.basic in user_roles
    assert Roles.admin not in user_roles


async def test_create_user_by_other_authorized_user(client: TestClient, db_session, authorized_user, fake):
    user_data = get_user_data(fake)
    users = await User.get_users(db_session)
    expected_user_count = len(users)
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt)
    response = await client.post("users", json=user_data)
    assert response.status == 403
    assert await User.get_or_none(db_session, **{"login": user_data.get("login")}) is None
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count


async def test_create_user_by_unauthorized_user(client: TestClient, db_session, main_user, fake):
    user_data = get_user_data(fake)
    users = await User.get_users(db_session)
    expected_user_count = len(users)
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt)
    response = await client.post("users", json=user_data)
    assert response.status == 403
    assert await User.get_or_none(db_session, **{"login": user_data.get("login")}) is None
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count


async def test_create_user_if_short_password_by_authorized_admin(client: TestClient, db_session, authorized_admin, fake):
    user_data = get_user_data(fake, password=fake.password(length=4))
    users = await User.get_users(db_session)
    expected_user_count = len(users)
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt)
    response = await client.post("users", json=user_data)
    assert response.status == 400
    assert await response.text() == "Invalid request data"
    assert await User.get_or_none(db_session, **{"login": user_data.get("login")}) is None
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count


async def test_create_user_if_wrong_birthday_by_authorized_admin(client: TestClient, db_session, authorized_admin, fake):
    user_data = get_user_data(fake, birthday=str(fake.date_of_birth(minimum_age=20, maximum_age=80).year))
    users = await User.get_users(db_session)
    expected_user_count = len(users)
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_user_role_count = await db_session.scalar(stmt)
    response = await client.post("users", json=user_data)
    assert response.status == 400
    assert await response.text() == "Invalid request data"
    assert await User.get_or_none(db_session, **{"login": user_data.get("login")}) is None
    assert len(await User.get_users(db_session)) == expected_user_count
    assert await db_session.scalar(stmt) == expected_user_role_count
