import pytest
from aiohttp.test_utils import TestClient
from sqlalchemy.future import select
from sqlalchemy.sql.functions import count

from db.models import User, user_role
from tests.utils import get_user_data

pytestmark = [pytest.mark.asyncio, pytest.mark.auth]


async def test_successful_registration(client: TestClient, db_session, fake):
    user_data = get_user_data(fake)
    response = await client.post("register", json=user_data)
    assert response.status == 200
    data = await response.json()
    assert user_data.get("login") == data.get("login")
    assert await User.exists(db_session, data.get("id")) is True


async def test_duplicated_registration(client: TestClient, db_session, fake):
    user_data = get_user_data(fake)
    response = await client.post("register", json=user_data)
    assert response.status == 200
    data = await response.json()
    assert user_data.get("login") == data.get("login")
    assert await User.exists(db_session, data.get("id")) is True

    users = await User.get_users(db_session)
    stmt = select(count(user_role.c.id)).where(user_role.c.user_id.in_([u.id for u in users]))
    expected_users_count = len(users)
    expected_user_role_count = await db_session.scalar(stmt)
    response = await client.post("register", json=user_data)
    assert response.status == 400
    assert await response.text() == "User with provided login already exists"
    assert len(await User.get_users(db_session)) == expected_users_count
    assert await db_session.scalar(stmt) == expected_user_role_count


async def test_registration_if_short_password(client: TestClient, db_session, fake):
    user_data = get_user_data(fake, password=fake.password(length=4))
    expected_users_count = len(await User.get_users(db_session))
    response = await client.post("register", json=user_data)
    assert response.status == 400
    assert await response.text() == "Invalid request data"
    assert len(await User.get_users(db_session)) == expected_users_count


async def test_registration_if_wrong_birthday(client: TestClient, db_session, fake):
    user_data = get_user_data(fake, birthday=str(fake.date_of_birth(minimum_age=20, maximum_age=80).year))
    expected_users_count = len(await User.get_users(db_session))
    response = await client.post("register", json=user_data)
    assert response.status == 400
    assert await response.text() == "Invalid request data"
    assert len(await User.get_users(db_session)) == expected_users_count
