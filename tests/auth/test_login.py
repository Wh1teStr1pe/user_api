import pytest
from aiohttp.test_utils import TestClient

from api import schemas

pytestmark = [pytest.mark.asyncio, pytest.mark.auth]


async def test_successful_login(client: TestClient, db_session, main_user):
    user_data = schemas.UserLogIn(login=main_user.login, password="admin")
    response = await client.post("login", json=user_data.dict())
    data = await response.json()
    assert response.status == 200
    assert data.get("login") == main_user.login


async def test_login_if_wrong_password(client: TestClient, db_session, main_user, fake):
    user_data = schemas.UserLogIn(login=main_user.login, password=fake.password())
    response = await client.post("login", json=user_data.dict())
    assert response.status == 403
    assert await response.text() == "Wrong password"


async def test_login_if_wrong_username(client: TestClient, db_session, main_user, fake):
    user_data = schemas.UserLogIn(login=fake.user_name(), password=fake.password())
    response = await client.post("login", json=user_data.dict())
    assert response.status == 403
    assert await response.text() == "User with provided login doesn't exists"
